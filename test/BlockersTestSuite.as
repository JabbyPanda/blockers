/**
 * Created by Jabbypanda on 24.03.2014.
 */
package
{
	import org.jabbypanda.model.TableModelTest;
	import org.jabbypanda.model.tile.TilesModelTest;

	[Suite]
	[RunWith("org.flexunit.runners.Suite")]
	public class BlockersTestSuite
	{
		public var tilesModelTest:TilesModelTest;

		public var gameTableModelTest:TableModelTest;
	}
}
