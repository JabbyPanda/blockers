/**
 * Created by Jabbypanda on 24.03.2014.
 */
package org.jabbypanda.model
{
	import flexunit.framework.Assert;

	import org.jabbypanda.model.tile.types.ITile;

	public class TableModelTest
	{
		private var gameTableModel:TableModel;

		[Before]
		public function setUp():void
		{
			gameTableModel = new TableModel();
		}

		[After]
		public function tearDown():void
		{
			gameTableModel = null;
		}

		[Test]
		public function testRows():void
		{
			Assert.assertEquals(4, gameTableModel.rows);
		}

		[Test]
		public function testColumns():void
		{
			Assert.assertEquals(4, gameTableModel.columns);
		}

		[Test]
		public function testCells():void
		{
			var rowCount:int = gameTableModel.cells.length;
			var columnCount:int = gameTableModel.cells[0].length;

			var cell:ITile = gameTableModel.cells[0][0] as ITile;

			Assert.assertEquals(4, rowCount);
			Assert.assertEquals(4, columnCount);
			Assert.assertEquals(true, cell.visible);
		}
	}
}
