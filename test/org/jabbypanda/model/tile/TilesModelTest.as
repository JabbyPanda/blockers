/**
 * Created by Jabbypanda on 24.03.2014.
 */
package org.jabbypanda.model.tile
{
	import flash.events.Event;

	import flexunit.framework.Assert;

	import org.flexunit.async.Async;

	import org.jabbypanda.model.tile.types.ITile;
	import org.jabbypanda.model.tile.types.TileTypes;

	public class TilesModelTest
	{
		private var tilesModel:TilesModel;

		private const TILES_COUNT:int = 16;

		[Before]
		public function setUp():void
		{
			tilesModel = new TilesModel(TILES_COUNT);
		}

		[After]
		public function tearDown():void
		{
			tilesModel = null;
		}

		[Test(async)]
		public function testOneTilePairFound():void
		{
			selectTilePair(TileTypes.CIRCLE);

			Async.handleEvent(this, tilesModel, TilesModel.TILE_PAIR_FOUND, onOneTilePairFound);
		}

		private function onOneTilePairFound(event:Event, args:*):void
		{
			Assert.assertEquals(2, tilesModel.foundTiles.length);
		}

		[Test(async)]
		public function testTwoTilePairFound():void
		{
			selectTilePair(TileTypes.CIRCLE);
			selectTilePair(TileTypes.DIAMOND);

			Async.handleEvent(this, tilesModel, TilesModel.TILE_PAIR_FOUND, onTwoTilePairsFound);
		}

		private function onTwoTilePairsFound(event:Event, args:*):void
		{
			Assert.assertEquals(4, tilesModel.foundTiles.length);
		}

		[Test(async)]
		public function testAllTilesFound():void
		{
			var tileTypesCount:int = TileTypes.ALL.length;
			var tileTypeIndex:int = 0;
			var currentTileIndex:int = 0;
			while (currentTileIndex < tilesModel.tilesCount)
			{
				selectTilePair(TileTypes.ALL[tileTypeIndex]);
				currentTileIndex += 2;
				tileTypeIndex++;

				if (tileTypeIndex == tileTypesCount)
				{
					tileTypeIndex = 0;
				}
			}

			Async.handleEvent(this, tilesModel, TilesModel.ALL_TILES_FOUND, onAllTilesFound);
		}

		private function onAllTilesFound(event:Event, args:*):void
		{
			Assert.assertEquals(TILES_COUNT, tilesModel.foundTiles.length);
		}

		[Test(async)]
		public function testIncorrectTilePairSelected():void
		{
			selectTile(TileTypes.CIRCLE);
			selectTile(TileTypes.DIAMOND);

			Async.handleEvent(this, tilesModel,
							  TilesModel.TILE_PAIR_INCORRECT, onIncorrectTilePairSelected, 4000,
							  [], onDefaultTimeoutHandler);
		}

		private function onIncorrectTilePairSelected(event:Event, args:*):void
		{
			Assert.assertEquals(0, tilesModel.foundTiles.length);
		}

		[Test(async)]
		public function testSameTileSelectedAndUnSelected():void
		{
			selectTile(TileTypes.CIRCLE);
			unselectTile(TileTypes.CIRCLE);

			Assert.assertEquals(0, tilesModel.foundTiles.length);
		}

		[Test(async)]
		public function testReset():void
		{
			selectTilePair(TileTypes.CIRCLE);
			selectTilePair(TileTypes.DIAMOND);

			Async.handleEvent(this, tilesModel, TilesModel.TILE_PAIR_FOUND, onTwoTilePairsFoundAndReset);

		}

		private function onTwoTilePairsFoundAndReset(event:Event, args:*):void
		{
			tilesModel.reset();
			Assert.assertEquals(0, tilesModel.foundTiles.length);
		}

		private function selectTilePair(tileType:String)
		{
			selectTile(tileType, 2);
		}

		private function selectTile(tileType:String, tileCountToBeFound:int = 1):void
		{
			var tileCountFound:int = 0;

			for (var tileIndex:int = 0; tileIndex < tilesModel.tilesCount; tileIndex++)
			{
				var tile:ITile = tilesModel.getTileByIndex(tileIndex) as ITile;
				if (tile.type == tileType)
				{
					tile.selected = true;
					tileCountFound++;

					if (tileCountFound == tileCountToBeFound)
					{
						return;
					}
				}
			}
		}

		private function unselectTile(tileType:String, tileCountToBeFound:int = 1):void
		{
			var tileCountFound:int = 0;

			for (var tileIndex:int = 0; tileIndex < tilesModel.tilesCount; tileIndex++)
			{
				var tile:ITile = tilesModel.getTileByIndex(tileIndex) as ITile;
				if (tile.type == tileType)
				{
					tile.selected = false;
					tileCountFound++;

					if (tileCountFound == tileCountToBeFound)
					{
						return;
					}
				}
			}
		}

		private function onDefaultTimeoutHandler(event:Event):void
		{
			trace ("Event timeout had occurred");
		}
	}
}
