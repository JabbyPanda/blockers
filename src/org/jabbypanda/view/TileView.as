/**
 * Created by Jabbypanda on 22.03.2014.
 */
package org.jabbypanda.view
{
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;

	import org.jabbypanda.model.tile.types.BaseTile;
	import org.jabbypanda.model.tile.types.ITile;

	public class TileView extends Sprite implements IView, ITileData
	{
		private const TILE_WIDTH:int  = 100;

		private const TILE_HEIGHT:int  = 100;

		private var tileWidth:int;

		private var tileHeight:int;

		private var frontImage:Bitmap;

		private var backImage:Bitmap;

		public function TileView()
		{
			tileWidth = TILE_WIDTH;
			tileHeight = TILE_HEIGHT;

			addEventListener(MouseEvent.CLICK, mouseClickHandler);

			useHandCursor = true;
			buttonMode = true;
			mouseChildren = false;
		}

		private var _data:ITile;

		public function get data():ITile
		{
			return _data;
		}

		public function set data(value:ITile):void
		{
			if (data != value)
			{
				if (data)
				{
					data.removeEventListener(BaseTile.VISIBLE_EVENT, tileVisibleHandler);
				}

				_data = value;
				if (data)
				{
					data.addEventListener(BaseTile.VISIBLE_EVENT, tileVisibleHandler);
					init();
				}
			}
		}

		public function init():void
		{
			render();
		}

		public function render():void
		{
			if (!data)
			{
				return;
			}

			if (frontImage)
			{
				removeChild(frontImage);
				frontImage = null;
			}

			if (backImage)
			{
				removeChild(backImage);
				backImage = null;
			}

			frontImage = createImage(data.frontImage);
			frontImage.visible = false;

			backImage = createImage(data.backImage);
			backImage.visible = true;
		}

		private function createImage(image:Bitmap):Bitmap
		{
			image.width = tileWidth;
			image.height = tileHeight;
			addChild(image);

			return image;
		}

		private function showFrontSide():void
		{
			data.selected = true;
		}

		private function hideFrontSide():void
		{
			data.selected = false;
		}

		/*
			Event handlers		
		 */
		private function tileVisibleHandler(event:Event):void
		{
			var tile:ITile = event.target as ITile;
			this.visible = tile.visible;
		}

		private function mouseClickHandler(event:MouseEvent):void
		{
			if (!data.selected)
			{
				showFrontSide();
			}
			else
			{
				hideFrontSide();
			}
		}
	}
}
