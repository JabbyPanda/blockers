/**
 * Created by Jabbypanda on 23.03.2014.
 */
package org.jabbypanda.view
{
	public interface IView
	{
		function init():void;

		function render():void;
	}
}
