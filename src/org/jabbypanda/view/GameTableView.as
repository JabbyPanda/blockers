/**
 * Created by Jabbypanda on 22.03.2014.
 */
package org.jabbypanda.view
{
	import flash.display.Sprite;

	import org.jabbypanda.layoyt.GridLayout;
	import org.jabbypanda.layoyt.ILayout;
	import org.jabbypanda.model.TableModel;
	import org.jabbypanda.model.tile.types.ITile;

	public class GameTableView extends Sprite implements IView
	{
		protected var uiLayout:ILayout;

		protected var uiElements:Array;

		protected var gameTableModel:TableModel;

		public function GameTableView()
		{
			gameTableModel = new TableModel();
			uiLayout = new GridLayout(gameTableModel.rows, gameTableModel.columns);
			init();
		}

		public function init():void
		{
			gameTableModel.init();
			render();
		}

		public function render():void
		{
			while (numChildren > 0)
			{
				removeChildAt(0);
			}

			uiElements = createTiles(gameTableModel.rows, gameTableModel.columns);
			updateTilesView(gameTableModel.rows, gameTableModel.columns);

			uiLayout.elements = uiElements;
		}

		private function createTiles(rowCount:int, columnCount:int):Array
		{
			var tiles:Array = [];
			var tileView:TileView;
			for (var rowIndex:int = 0; rowIndex < rowCount; rowIndex++)
			{
				for (var columnIndex:int = 0; columnIndex < columnCount; columnIndex++)
				{
					tileView = new TileView();
					tiles.push(tileView);

					addChild(tileView);
				}
			}

			return tiles;
		}

		private function updateTilesView(rowCount:int, columnCount:int):void
		{
			for (var rowIndex:int = 0; rowIndex < rowCount; rowIndex++)
			{
				for (var columnIndex:int = 0; columnIndex < columnCount; columnIndex++)
				{
					var elementIndex:int = (rowIndex * columnCount) +  columnIndex;
					var tileView:ITileData = uiElements[elementIndex] as ITileData;
					if (tileView)
					{
						var tileData:ITile = ITile(gameTableModel.cells[rowIndex][columnIndex]);
						tileView.data = tileData;
					}
				}
			}
		}
	}
}
