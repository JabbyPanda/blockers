/**
 * Created by Jabbypanda on 23.03.2014.
 */
package org.jabbypanda.view
{
	import org.jabbypanda.model.tile.types.ITile;

	public interface ITileData
	{
		function get data():ITile

		function set data(value:ITile):void
	}
}
