/**
 * Created by Jabbypanda on 22.03.2014.
 */
package org.jabbypanda.view.ui
{
	import flash.display.Sprite;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;

	[Event(name="click", type="flash.events.MouseEvent")]
	public class Button extends Sprite
	{
		private var textField:TextField;

		private var label:String;

		private var buttonWidth:int;

		private var buttonHeight:int;

		public function Button(label:String, width:Number, height:Number)
		{
			this.label = label;

			render(width, height);

			useHandCursor = true;
			buttonMode = true;
			mouseChildren = false;
		}

		public function render(width:Number, height:Number):void
		{
			buttonWidth = width;
			buttonHeight = height;

			addLabel();
			drawBackground();
			positionLabel();
		}

		private function addLabel():void
		{
			textField = new TextField();
			textField.textColor = 0xFFFFFF;
			textField.text = label;
			textField.width = buttonWidth;
			textField.height = buttonHeight;

			var format:TextFormat = new TextFormat();
			format.size = 12;
			format.align = TextFormatAlign.CENTER;
			format.font = "Arial";

			textField.setTextFormat(format);
			addChild(textField);
		}

		private function drawBackground():void
		{
			//Set the color of the button graphic
			graphics.beginFill(0x000000);

			//Set the X,Y, Width, and Height of the button graphic
			graphics.drawRect(0, 0, buttonWidth, buttonHeight);

			//Apply the fill
			graphics.endFill();
		}

		private function positionLabel():void
		{
			textField.x = (width - textField.width) /2;
			textField.y = (height - textField.height) /2;
		}
	}
}
