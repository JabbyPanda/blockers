/**
 * Created by Jabbypanda on 22.03.2014.
 */
package org.jabbypanda.model
{
	import org.jabbypanda.model.tile.types.ITile;
	import org.jabbypanda.model.tile.TilesModel;

	public class TableModel
	{
		private const ROW_COUNT:int = 4;

		private const COLUMN_COUNT:int = 4;

		private var tilesModel:TilesModel;

		public function TableModel()
		{
			_rows = ROW_COUNT;
			_columns = COLUMN_COUNT;

			init();
		}

		private var _cells:Array;

		//2 dimensional array of table cells
		public function get cells():Array
		{
			return _cells;
		}

		private var _rows:Number = 0;

		public function get rows():Number
		{
			return _rows;
		}

		private var _columns:Number = 0;

		public function get columns():Number
		{
			return _columns;
		}

		public function init():void
		{
			if (!tilesModel)
			{
				var tilesCount:int = rows * columns;
				tilesModel = new TilesModel(tilesCount);
			}

			tilesModel.reset();
			_cells = createCells(rows, columns);
		}

		private function createCells(rowCount:int, columnCount:int):Array
		{
			var tiles:Array = [];
			for (var rowIndex:int = 0; rowIndex < rowCount; rowIndex++)
			{
				var row:Array = createRow(columnCount, rowIndex);
				tiles.push(row)
			}
			return tiles;
		}

		private function createRow(columnCount:int, rowIndex:int):Array
		{
			var row:Array = [];
			for (var columnIndex:int = 0; columnIndex < columnCount; columnIndex++)
			{
				var cellIndex:int = (rowIndex * columnCount) +  columnIndex;
				var tile:ITile = tilesModel.getTileByIndex(cellIndex);
				row.push(tile);

			}
			return row;
		}
	}
}
