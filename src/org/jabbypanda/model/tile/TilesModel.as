/**
 * Created by Jabbypanda on 22.03.2014.
 */
package org.jabbypanda.model.tile
{
	import flash.events.Event;
	import flash.events.EventDispatcher;

	import org.jabbypanda.model.tile.types.BaseTile;
	import org.jabbypanda.model.tile.types.ITile;
	import org.jabbypanda.model.tile.types.TileFactory;
	import org.jabbypanda.model.tile.types.TileTypes;
	import org.jabbypanda.util.DelayedCallManager;

	public class TilesModel extends EventDispatcher
	{
		public static const TILE_SELECT:String = "tileSelect";

		public static const ALL_TILES_FOUND:String = "allTilesFound";

		public static const TILE_PAIR_FOUND:String = "tilePairFound";

		public static const TILE_PAIR_INCORRECT:String = "tilePairIncorrect";

		private var tiles:Array;

		private var lastMatchingPair:Array;

		private var firstSelectedTile:ITile;

		private var secondSelectedTile:ITile;

		public function TilesModel(tilesCount:int)
		{
			_tilesCount = tilesCount;
			reset();
		}

		private var _tilesCount:int;

		public function get tilesCount():int
		{
			return _tilesCount;
		}

		private var _foundTiles:Array;

		public function get foundTiles():Array
		{
			return _foundTiles;
		}

		public function getTileByIndex(index:int):ITile
		{
			if (!tiles || index < 0 || index > tiles.length)
			{
				return null;
			}

			return ITile(tiles[index]);
		}

		public function reset():void
		{
			lastMatchingPair = [];
			_foundTiles = [];
			prepare();
			shuffle();
		}

		private function prepare():void
		{
			tiles = [];

			var tileTypeIndex:int = 0;
			for (var tileIndex:int = 0; tileIndex < _tilesCount; tileIndex++)
			{
				//create a 2 tiles of the same type
				if (tileIndex != 0 && tileIndex % 2 == 0)
				{
					tileTypeIndex++;

					if (tileTypeIndex == TileTypes.ALL.length)
					{
						tileTypeIndex = 0;
					}
				}

				var tileType:String = TileTypes.ALL[tileTypeIndex];
				var tile:ITile = TileFactory.create(tileIndex, tileType);
				tile.addEventListener(BaseTile.SELECTED_EVENT, onTileSelected);

				tiles.push(tile);
			}
		}

		private function shuffle():void
		{
			tiles.sort(randomize);
		}

		private function randomize (a:*, b:*):int
		{
			return ( Math.random() > .5 ) ? 1 : -1;
		}

		private function get bothTilesSelected():Boolean
		{
			return firstSelectedTile &&
				   firstSelectedTile.selected &&
				   secondSelectedTile &&
				   secondSelectedTile.selected;
		}

		private function processTileSelection(tile:ITile):void
		{
			var thirdTileSelected:Boolean = bothTilesSelected && tile;

			if (!firstSelectedTile)
			{
				firstSelectedTile = tile;
			}
			else if (firstSelectedTile == tile)
			{
				processTileDeselection(tile);
			}
			else if (!secondSelectedTile)
			{
				secondSelectedTile = tile;
			}

			if (bothTilesSelected)
			{
				var isMatchingPair:Boolean = (firstSelectedTile.type == secondSelectedTile.type);
				if (isMatchingPair)
				{
					lastMatchingPair = [];
					lastMatchingPair.push(firstSelectedTile);
					lastMatchingPair.push(secondSelectedTile);

					if (thirdTileSelected)
					{
						addMatchingPair();
						firstSelectedTile = tile;
					}
					else
					{
						DelayedCallManager.impl.delay(addMatchingPair, [], 500);
					}
				}
				else
				{
					if (thirdTileSelected)
					{
						resetSelectedPair();
						firstSelectedTile = tile;
					}
					else
					{
						DelayedCallManager.impl.delay(resetSelectedPair, [], 2000);
					}
				}
			}
		}

		private function processTileDeselection(tile:ITile):void
		{
			if (firstSelectedTile == tile)
			{
				firstSelectedTile = null;
			}
			else if (secondSelectedTile == tile)
			{
				secondSelectedTile = null;
			}
		}

		private function addMatchingPair():void
		{
			DelayedCallManager.impl.reset(addMatchingPair);

			for each (var tile:ITile in lastMatchingPair)
			{
				tile.visible = false;
				_foundTiles.push(tile);
			}

			resetSelectedPair();

			dispatchEvent(new Event(TILE_PAIR_FOUND));

			if (foundTiles.length == _tilesCount)
			{
				trace ("End of the game");
				dispatchEvent(new Event(ALL_TILES_FOUND));
			}
		}

		private function resetSelectedPair():void
		{
			DelayedCallManager.impl.reset(resetSelectedPair);

			if (firstSelectedTile)
			{
				firstSelectedTile.selected = false;
				firstSelectedTile = null;
			}

			if (secondSelectedTile)
			{
				secondSelectedTile.selected = false;
				secondSelectedTile = null;
			}

			dispatchEvent(new Event(TILE_PAIR_INCORRECT));
		}

		/*
			Event handler
		 */
		private function onTileSelected(event:Event):void
		{
			var tile:ITile = event.target as ITile;

			if (!tile)
			{
				return;
			}

			if (tile.selected)
			{
				processTileSelection(tile);
			}
			else
			{
				processTileDeselection(tile);
			}

			dispatchEvent(new Event(TILE_SELECT));
		}
	}
}
