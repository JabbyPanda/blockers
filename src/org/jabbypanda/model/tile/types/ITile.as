/**
 * Created by Jabbypanda on 22.03.2014.
 */
package org.jabbypanda.model.tile.types
{
	import flash.display.Bitmap;
	import flash.events.IEventDispatcher;

	public interface ITile extends IEventDispatcher
	{
		function get selected():Boolean;

		function set selected(value:Boolean):void

		function get visible():Boolean;

		function set visible(value:Boolean):void;

		function get frontImage():Bitmap;

		function get backImage():Bitmap;

		function get type():String;
	}
}
