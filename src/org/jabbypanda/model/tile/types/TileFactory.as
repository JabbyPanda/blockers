/**
 * Created by Jabbypanda on 22.03.2014.
 */
package org.jabbypanda.model.tile.types
{
	public class TileFactory
	{
		public static function create(index:int, type:String):ITile
		{
			switch (type)
			{
				case TileTypes.CIRCLE:
				{
					return new CircleTile(index);
				}

				case TileTypes.DIAMOND:
				{
					return new DiamondTile(index);
				}

				case TileTypes.HEXAHENDRON:
				{
					return new HexahedronTile(index);
				}


				case TileTypes.STAR:
				{
					return new StarTile(index);
				}

				case TileTypes.TRIANGLE:
				{
					return new TriangleTile(index);
				}
			}

			return null;
		}
	}
}
