/**
 * Created by Jabbypanda on 22.03.2014.
 */
package org.jabbypanda.model.tile.types
{
	import flash.display.Bitmap;

	import org.jabbypanda.Assets;

	public class HexahedronTile extends BaseTile
	{
		public function HexahedronTile(index:int)
		{
			super(index);
			tileType = TileTypes.HEXAHENDRON;
			frontImage = new Assets.HexahedronImage() as Bitmap;
		}
	}
}
