/**
 * Created by Jabbypanda on 22.03.2014.
 */
package org.jabbypanda.model.tile.types
{
	public class TileTypes
	{
		public static const CIRCLE:String = "circle";

		public static const DIAMOND:String = "diamond";

		public static const HEXAHENDRON:String = "hexahedron";

		public static const STAR:String = "star";

		public static const TRIANGLE:String = "triangle";


		public static const ALL:Array = [TileTypes.CIRCLE,
										 TileTypes.DIAMOND,
										 TileTypes.HEXAHENDRON,
										 TileTypes.STAR,
										 TileTypes.TRIANGLE];
	}
}
