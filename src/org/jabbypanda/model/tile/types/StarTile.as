/**
 * Created by Jabbypanda on 22.03.2014.
 */
package org.jabbypanda.model.tile.types
{
	import flash.display.Bitmap;

	import org.jabbypanda.Assets;

	public class StarTile extends BaseTile
	{
		public function StarTile(index:int)
		{
			super(index);
			tileType = TileTypes.STAR;
			frontImage = new Assets.StarImage() as Bitmap;
		}
	}
}
