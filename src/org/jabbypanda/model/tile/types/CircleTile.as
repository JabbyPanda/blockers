/**
 * Created by Jabbypanda on 22.03.2014.
 */
package org.jabbypanda.model.tile.types
{
	import flash.display.Bitmap;

	import org.jabbypanda.Assets;

	public class CircleTile extends BaseTile
	{
		public function CircleTile(index:int)
		{
			super(index);
			tileType = TileTypes.CIRCLE;
			frontImage = new Assets.CircleImage() as Bitmap;
		}
	}
}
