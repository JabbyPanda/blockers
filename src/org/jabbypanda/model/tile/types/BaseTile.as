/**
 * Created by Jabbypanda on 22.03.2014.
 */
package org.jabbypanda.model.tile.types
{
	import flash.display.Bitmap;
	import flash.events.Event;
	import flash.events.EventDispatcher;

	import org.jabbypanda.Assets;

	[Event(name="selected", type="flash.events.Event")]
	[Event(name="visible", type="flash.events.Event")]
	public class BaseTile extends EventDispatcher implements ITile
	{
		public static const SELECTED_EVENT:String = "selected";

		public static const VISIBLE_EVENT:String = "visible";

		private var index:int;

		public function BaseTile(index:int)
		{
			this.index = index;

			_backImage = new Assets.QuestionMarkImage() as Bitmap;
		}

		protected var tileType:String;

		public function get type():String
		{
			return tileType;
		}

		private var _frontImage:Bitmap;

		public function get frontImage():Bitmap
		{
			return _frontImage;
		}

		public function set frontImage(value:Bitmap):void
		{
			_frontImage = value;
		}

		private var _backImage:Bitmap;

		public function get backImage():Bitmap
		{
			return _backImage;
		}

		private var _selected:Boolean;

		public function set selected(value:Boolean):void
		{
			if (selected != value)
			{
				swapImage();
				_selected = value;
				dispatchEvent(new Event(SELECTED_EVENT));
			}
		}

		public function get selected():Boolean
		{
			return _selected;
		}

		private var _visible:Boolean = true;

		public function get visible():Boolean
		{
			return _visible;
		}

		public function set visible(value:Boolean):void
		{
			if (visible != value)
			{
				_visible = value;
				dispatchEvent(new Event(VISIBLE_EVENT));
			}
		}


		private function swapImage():void
		{
			frontImage.visible = !frontImage.visible;
			backImage.visible = !backImage.visible;
		}

	}
}
