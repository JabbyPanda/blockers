/**
 * Created by Jabbypanda on 22.03.2014.
 */
package org.jabbypanda.model.tile.types
{
	import flash.display.Bitmap;

	import org.jabbypanda.Assets;

	public class TriangleTile extends BaseTile
	{
		public function TriangleTile(index:int)
		{
			super(index);
			tileType = TileTypes.TRIANGLE;
			frontImage = new Assets.TriangleImage() as Bitmap;
		}
	}
}
