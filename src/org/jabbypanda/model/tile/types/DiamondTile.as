/**
 * Created by Jabbypanda on 22.03.2014.
 */
package org.jabbypanda.model.tile.types
{
	import flash.display.Bitmap;

	import org.jabbypanda.Assets;

	public class DiamondTile extends BaseTile
	{
		public function DiamondTile(index:int)
		{
			super(index);
			tileType = TileTypes.DIAMOND;
			frontImage = new Assets.DiamondImage() as Bitmap;
		}
	}
}
