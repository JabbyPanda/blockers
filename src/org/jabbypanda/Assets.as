/**
 * Created by Jabbypanda on 22.03.2014.
 */
package org.jabbypanda
{
	public class Assets
	{
		[Embed(source="/assets/0.png")]
		public static var QuestionMarkImage:Class;

		[Embed(source="/assets/1.png")]
		public static var DiamondImage:Class;

		[Embed(source="/assets/2.png")]
		public static var CircleImage:Class;

		[Embed(source="/assets/3.png")]
		public static var StarImage:Class;

		[Embed(source="/assets/4.png")]
		public static var HexahedronImage:Class;

		[Embed(source="/assets/5.png")]
		public static var TriangleImage:Class;
	}
}
