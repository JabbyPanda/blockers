/**
 * Created by Jabbypanda on 22.03.2014.
 */
package org.jabbypanda.util
{
	import flash.events.TimerEvent;
	import flash.utils.Timer;

	public class DelayedCallManager
	{
		private var delayedFunctions:Array = [];

		private static var _instance:DelayedCallManager;

		public function DelayedCallManager(e:Enforcer)
		{
			if (e == null)
			{
				throw new Error("Cannot create new instance of this class");
			}

			delayedFunctions = [];
		}

		public static function get impl():DelayedCallManager
		{
			if(_instance == null)
			{
				_instance = new DelayedCallManager(new Enforcer);
			}
			return _instance;
		}

		public function reset(functionCall:Function):void
		{
			if (!isDelayed(functionCall))
			{
				return;
			}

			var functionIndex:int = delayedFunctions.indexOf(functionCall);
			delete delayedFunctions[functionIndex];
		}

		public function isDelayed(functionCall:Function):Boolean
		{
			return delayedFunctions.indexOf(functionCall) != -1
		}

		public function delay(functionCall:Function, params:Array, delayTime:int):void
		{
			var f:Function;
			var timer:Timer = new Timer(delayTime, 1);

			delayedFunctions.push(functionCall);
			timer.addEventListener(TimerEvent.TIMER,
					f = function():void
					{
						if (isDelayed(functionCall))
						{
							reset(functionCall);
							functionCall.apply(null, params);
						}

						timer.removeEventListener(TimerEvent.TIMER, f);
						timer = null;
					});
			timer.start();
		}
	}
}

// an empty, private class
class Enforcer{}
