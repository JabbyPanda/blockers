/**
 * Created by Jabbypanda on 22.03.2014.
 */
package org.jabbypanda.layoyt
{
	public class BaseLayout implements ILayout
	{
		private var _elements:Array;

		public function set elements(value:Array):void
		{
			_elements = value;
			layout();
		}

		public function get elements():Array
		{
			return _elements;
		}

		//abstract method
		public function layout():void
		{
			// to be overrided in descendants
		}
	}
}
