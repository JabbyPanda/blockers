/**
 * Created by Jabbypanda on 22.03.2014.
 */
package org.jabbypanda.layoyt
{
	import flash.display.Sprite;

	public class VerticalLayout extends BaseLayout
	{
		private var paddingLeft:int = 10;

		private var paddingTop:int = 10;

		private var gap:int = 10;

		override public function layout():void
		{
			var elementsCount:int = elements.length;

			var x:int = paddingLeft;
			var y:int = paddingTop;

			for (var i:int = 0; i < elementsCount; i++)
			{
				var element:Sprite = elements[i];
				element.x = x;
				element.y = y;

				y += element.height + gap;
			}
		}
	}
}
