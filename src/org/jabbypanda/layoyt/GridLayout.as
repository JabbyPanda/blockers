/**
 * Created by Jabbypanda on 22.03.2014.
 */
package org.jabbypanda.layoyt
{
	import flash.display.Sprite;

	public class GridLayout extends BaseLayout
	{
		public var horizontalGap:int = 10;

		public var verticalGap:int = 10;

		private var paddingLeft:int = 10;

		private var paddingTop:int = 0;

		private var rowCount:int;

		private var columnCount:int;

		public function GridLayout(rowCount:int, columnCount:int)
		{
			this.rowCount = rowCount;
			this.columnCount = columnCount;
		}

		override public function layout():void
		{
			var x:int = paddingLeft;
			var y:int = paddingTop;

			var element:Sprite;
			var elementIndex:int = 0;
			for (var rowIndex:int = 0; rowIndex < rowCount; rowIndex++)
			{
				x = 0;
				for (var columnIndex:int = 0; columnIndex < columnCount; columnIndex++)
				{
					element = elements[elementIndex];
					element.y = y;
					element.x = x;
					x+= element.width + horizontalGap;
					elementIndex++;
				}
				y+= element.height + verticalGap;
			}
		}
	}
}
