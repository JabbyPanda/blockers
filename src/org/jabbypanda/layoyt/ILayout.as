/**
 * Created by Jabbypanda on 22.03.2014.
 */
package org.jabbypanda.layoyt
{
	public interface ILayout
	{
		function layout():void;

		function set elements(value:Array):void;

		function get elements():Array;
	}
}
