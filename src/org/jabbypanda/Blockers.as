/**
 * Created by Jabbypanda on 22.03.2014.
 */
package org.jabbypanda
{
	import flash.display.Sprite;
	import flash.events.MouseEvent;

	import org.jabbypanda.layoyt.ILayout;
	import org.jabbypanda.layoyt.VerticalLayout;
	import org.jabbypanda.view.GameTableView;
	import org.jabbypanda.view.ui.Button;

	[SWF(width="450", height="490")]
	public class Blockers extends Sprite
	{
		protected var uiLayout:ILayout;

		protected var gameTableView:GameTableView;

		protected var actionButton:Button;

		public function Blockers()
		{
			uiLayout = new VerticalLayout();
			render();
		}

		private function render():void
		{
			actionButton = new Button("Restart", 430, 20);
			actionButton.addEventListener(MouseEvent.CLICK, buttonClickHandler);
			addChild(actionButton);

			gameTableView = new GameTableView();
			addChild(gameTableView);

			uiLayout.elements = [actionButton, gameTableView];
		}

		/*
			Event handlers
		 */
		private function buttonClickHandler(event:MouseEvent):void
		{
			gameTableView.init();
		}
	}
}
